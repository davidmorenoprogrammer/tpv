﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TPV
{
    public partial class platos : Form
    {
        ArrayList nombres = new ArrayList();
        ArrayList precios = new ArrayList();
        ArrayList cantidades = new ArrayList();
        public platos(ArrayList precios, ArrayList cantidades, ArrayList nombres)
        {
            InitializeComponent();
            this.precios = precios;
            this.cantidades = cantidades;
            this.nombres = nombres;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
            new hamburguesa(precios, cantidades, nombres).Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
            new Form2(precios,cantidades, nombres).Show();
        }

        private void platos_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
            new alitas(precios, cantidades, nombres).Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
            new papa(precios, cantidades, nombres).Show();
        }
    }
}
