﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TPV
{
    public partial class hamburguesa : Form
    {
        ArrayList nombres = new ArrayList();
        ArrayList precios = new ArrayList();
        ArrayList cantidades = new ArrayList();
        
        int numero_hamburguesa1;
        int numero_hamburguesa2;
        int numero_hamburguesa3;
        double precio_1;
        double precio_2;
        double precio_3;
        


        public hamburguesa(ArrayList precios, ArrayList cantidades, ArrayList nombres)
        {
            numero_hamburguesa1 = 1;
            numero_hamburguesa2 = 1;
            numero_hamburguesa3 = 1;
            precio_1 = 5.99;
            precio_2 = 6.99;
            precio_3 = 7.99;

            InitializeComponent();
            p1.Text = "" + precio_1;
            p2.Text = "" + precio_2;
            p3.Text = "" + precio_3;
            this.precios = precios;
            this.cantidades = cantidades;
            this.nombres = nombres;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void hamburguesa_Load(object sender, EventArgs e)
        {

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            numero_hamburguesa1++;
            label8.Text = "" + numero_hamburguesa1;
            p1.Text = "" + (numero_hamburguesa1 * 5.99);
            precio_1 = precio_1 + (numero_hamburguesa1 * 5.99);



        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (numero_hamburguesa1 > 0) {
                numero_hamburguesa1--;
                label8.Text = "" + numero_hamburguesa1;
                p1.Text = "" + (numero_hamburguesa1 * 5.99);
                precio_1 = precio_1 - (numero_hamburguesa1 * 5.99);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            numero_hamburguesa2++;
            label5.Text = "" + numero_hamburguesa2;
            p2.Text = "" + (numero_hamburguesa2 * 6.99);
            precio_2 = precio_2 + (numero_hamburguesa2 * 6.99);

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (numero_hamburguesa2 > 0)
            {
                numero_hamburguesa2--;
                label5.Text = "" + numero_hamburguesa2;
                p2.Text = "" + (numero_hamburguesa2 * 6.99);
                precio_2 = precio_2 - (numero_hamburguesa2 * 6.99);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            numero_hamburguesa3++;
            label10.Text = "" + numero_hamburguesa3;
            p3.Text = "" + (numero_hamburguesa3 * 7.99);
            precio_3 = precio_3 + (numero_hamburguesa3 * 7.99);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (numero_hamburguesa3 > 0)
            {
                numero_hamburguesa3--;
                label10.Text = "" + numero_hamburguesa3;
                p3.Text = "" + (numero_hamburguesa3 * 7.99);
                precio_3 = precio_3 - (numero_hamburguesa3 * 7.99);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.Close();
            new platos(precios, cantidades,nombres).Show();
        }

        private void addcarrito1_Click(object sender, EventArgs e)
        {
            precios.Add(precio_1);
            nombres.Add("BigMac");
            cantidades.Add(numero_hamburguesa1);

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            
          
        }

        private void button8_Click(object sender, EventArgs e)
        {
            this.Close();
            new carrito(precios,cantidades,nombres).Show();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            precios.Add(precio_2);
            nombres.Add("Krustyburguer");
            cantidades.Add(numero_hamburguesa2);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            precios.Add(precio_3);
            nombres.Add("Cangreburguer");
            cantidades.Add(numero_hamburguesa3);
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }
    }
}
