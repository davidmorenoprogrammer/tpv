﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TPV
{
    public partial class ofertas : Form
    {
        ArrayList nombres = new ArrayList();
        ArrayList precios = new ArrayList();
        ArrayList cantidades = new ArrayList();
        double precio_1;
        double precio_2;
        public ofertas(ArrayList precios, ArrayList cantidades, ArrayList nombres)
        {
            InitializeComponent();
            precio_1 = 5;
            precio_2 = 3.90;
            
            this.precios = precios;
            this.cantidades = cantidades;
            this.nombres = nombres;
        }

        private void addcarrito1_Click(object sender, EventArgs e)
        {
            precios.Add(precio_1);
            nombres.Add("Menu mas mac flury");
            cantidades.Add(1);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            precios.Add(precio_2);
            nombres.Add("Menu");
            cantidades.Add(5);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            this.Close();
            new Form2(precios, cantidades, nombres).Show();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            this.Close();
            new carrito(precios, cantidades, nombres).Show();
        }
    }
}
