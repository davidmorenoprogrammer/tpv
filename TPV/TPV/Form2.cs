﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TPV
{
    public partial class Form2 : Form
    {
        ArrayList nombres = new ArrayList();
        ArrayList precios = new ArrayList();
        ArrayList cantidades = new ArrayList();
        public Form2(ArrayList precios, ArrayList cantidades, ArrayList nombres)
        {
            InitializeComponent();
            this.precios = precios;
            this.cantidades = cantidades;
            this.nombres = nombres;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
            new hacerpedido().Show();
            
        }

        private void pedirprimerplato_Click(object sender, EventArgs e)
        {
            this.Close();
            new platos(precios, cantidades, nombres).Show();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
           
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
            new bebidas(precios, cantidades, nombres).Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
            new ofertas(precios, cantidades, nombres).Show();
        }
    }
}
